package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Service.ProjectService;

import java.text.ParseException;
import java.util.Map;

public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(final ProjectService projectService) {
        this.projectService = projectService;
    }

    public void showAll() {
        System.out.println("[ВАШИ ПРОЕКТЫ]");

        final Map<String, Project> projects = projectService.getAllProjects();

        for (Map.Entry<String, Project> projectEntry: projects.entrySet()) {
            System.out.println(projectEntry.getValue());
        }

        System.out.println("[OK]");
    }

    public void create(
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd
    ) {
        try {
            String projectId = projectService.createProject(name, description, dateStart, dateEnd);
            System.out.println("[ПРОЕКТ " + projectId + " СОЗДАН]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(
            final String projectId,
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd
    ) {

        try {
            projectService.updateProject(projectId, name, description, dateStart, dateEnd);
            System.out.println("[ПРОЕКТ " + projectId + " ИЗМЕНЕН]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void remove(final String id) {
        try {
            projectService.removeProjectById(id);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }


        System.out.println("[ПРОЕКТ #" + id + " УДАЛЕН]");
    }

    public void removeAll() {
        projectService.removeAllProjects();
    }

    public void generate() {
        int count = 1 + (int) (Math.random() * 8);

        for (int i = 1; i <= count; i++) {
            String name = "Проект #" + i;
            String description = "Описание проекта #" + i;
            String dateStart = "30/12/1900";
            String dateEnd = "30/12/1900";

            try {
                projectService.createProject(name, description, dateStart, dateEnd);
            } catch (IllegalArgumentException | NullPointerException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
