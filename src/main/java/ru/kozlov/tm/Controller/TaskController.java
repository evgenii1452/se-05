package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.ParseException;
import java.util.Map;

public class TaskController {

    private final TaskService taskService;
    private final ProjectService projectService;

    public TaskController(
            final TaskService taskService,
            final ProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public void create(
            String projectId,
            String name,
            String description,
            String dateStart,
            String dateEnd
    ) {
        try {
            String taskId = taskService.createTask(projectId, name, description, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА \"" + taskId + "\" СОЗДАНА]");
        } catch (NullPointerException | IllegalArgumentException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(
            String projectId,
            String taskId,
            String name,
            String description,
            String dateStart,
            String dateEnd
    ) {
        try {
            taskService.updateTask(projectId, taskId, name, description, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА " + taskId + " ИЗМЕНЕНА]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void showAll(String projectId) {
        try {
            Map<String, Task> tasks = taskService.getTasksByProjectId(projectId);

            System.out.println("Кол-во задач:" + tasks.size());

            for (Map.Entry<String, Task> taskEntry: tasks.entrySet()) {
                System.out.println(taskEntry.getValue());
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    public void remove(String id) {
        try {
            taskService.removeTaskById(id);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        System.out.println("[ЗАДАЧА #" + id + " УДАЛЕНА]");
    }

    public void removeAll(String projectId) {
        try {
            taskService.removeTasksByProjectId(projectId);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        taskService.removeTasksByProjectId(projectId);

        System.out.println("[ЗАДАЧИ ПРОЕКТА #" + projectId + " УДАЛЕНЫ]");
    }

    public void generate() {
        Map<String, Project> projects = projectService.getAllProjects();

        for (Map.Entry<String, Project> projectEntry : projects.entrySet()) {
            int count = 1 + (int) (Math.random() * 8);
            String projectId = projectEntry.getKey();

            for (int i = 1; i <= count; i++) {
                String taskName = "Задача #" + i;
                String taskDescription = "Описание задачи #" + i;
                String dateStart = "30/12/1900";
                String dateEnd = "30/12/1900";

                try {
                    taskService.createTask(projectId, taskName, taskDescription, dateStart, dateEnd);
                } catch (NullPointerException | IllegalArgumentException | ParseException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}
