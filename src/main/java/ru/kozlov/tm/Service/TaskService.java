package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Repository.TaskRepository;
import ru.kozlov.tm.Util.HelpUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class TaskService {
    private final TaskRepository taskRepository;

    public TaskService(final TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String createTask(
            final String projectId,
            final String taskName,
            final String taskDescription,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        stringValidation(projectId);
        stringValidation(taskName);
        stringValidation(taskDescription);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        final String taskId = UUID.randomUUID().toString();
        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Task task = new Task(taskId, taskName, taskDescription, dateStart, dateEnd, projectId);

        return taskRepository.persist(task);
    }

    public void updateTask(
            final String projectId,
            final String taskId,
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        stringValidation(taskId);
        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);
        stringValidation(taskId);

        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Task task = new Task(taskId, name, description, dateStart, dateEnd, projectId);

        taskRepository.merge(task);
    }

    public Map<String, Task> getTasksByProjectId(final String projectId) {
        stringValidation(projectId);

        return taskRepository.findAll(projectId);
    }

    public void removeTaskById(final String id) {
        stringValidation(id);

        final Task removedTask = taskRepository.remove(id);

        if (removedTask == null) {
            throw new IllegalArgumentException("[ЗАДАЧА НЕ НАЙДЕНА]");
        }
    }

    public void removeTasksByProjectId(final String projectId) {
        stringValidation(projectId);

        taskRepository.removeALl(projectId);
    }

    private void stringValidation(final String value) {
        if (HelpUtil.stringIsEmpty(value)) {
            throw new NullPointerException("[ПОЛЯ НЕ МОГУТ БЫТЬ ПУСТЫМИ]");
        }
    }
}
