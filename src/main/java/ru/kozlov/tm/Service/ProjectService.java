package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Repository.ProjectRepository;
import ru.kozlov.tm.Util.HelpUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class ProjectService {
    private final ProjectRepository projectRepository;
    private final TaskService taskService;

    public ProjectService(
            final ProjectRepository projectRepository,
            final TaskService taskService
    ) {
        this.projectRepository = projectRepository;
        this.taskService = taskService;
    }

    public void removeAllProjects() {
        projectRepository.removeAll();
    }

    public String createProject(
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        final String id = UUID.randomUUID().toString();

        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Project project = new Project(id, name, description, dateStart, dateEnd);

        return projectRepository.persist(project);
    }

    public Map<String, Project> getAllProjects() {
        return projectRepository.findAll();
    }

    public void removeProjectById(final String id) {
        stringValidation(id);
        checkProjectExists(id);

        taskService.removeTasksByProjectId(id);
        projectRepository.remove(id);

    }

    public Project getProjectById(final String id) {
        stringValidation(id);
        checkProjectExists(id);

        return projectRepository.find(id);
    }

    public void updateProject(
            final String projectId,
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        stringValidation(projectId);
        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Project project = new Project(projectId, name, description, dateStart, dateEnd);

        projectRepository.merge(project);
    }

    private void checkProjectExists(final String id) {
        if (!projectRepository.keyExists(id)) {
            throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
        }
    }

    private void stringValidation(final String value) {
        if (HelpUtil.stringIsEmpty(value)) {
            throw new NullPointerException("[ПОЛЯ НЕ МОГУТ БЫТЬ ПУСТЫМИ]");
        }
    }
}
