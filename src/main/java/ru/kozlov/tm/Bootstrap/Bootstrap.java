package ru.kozlov.tm.Bootstrap;

import ru.kozlov.tm.Command.*;
import ru.kozlov.tm.Controller.ProjectController;
import ru.kozlov.tm.Controller.TaskController;
import ru.kozlov.tm.Repository.ProjectRepository;
import ru.kozlov.tm.Repository.TaskRepository;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;
import ru.kozlov.tm.Util.HelpUtil;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Bootstrap {
    private final Scanner scanner = new Scanner(System.in);

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository, taskService);

    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskController taskController = new TaskController(taskService, projectService);

    private final Map<String,AbstractCommand> commandMap = new LinkedHashMap<>();

    public void registry(final AbstractCommand command) throws Exception {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();

        if (HelpUtil.stringIsEmpty(cliCommand)) {
            throw new Exception();
        }

        if (HelpUtil.stringIsEmpty(cliDescription)) {
            throw new Exception();
        }

        command.setBootstrap(this);

        commandMap.put(cliCommand, command);
    }

    public void init(final AbstractCommand[] commands) throws Exception {
        if (commands.length < 1) {
            throw new Exception();
        }

        for (AbstractCommand command : commands) {
            registry(command);
        }

        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";

        while (!command.equals("exit")){
            System.out.println("[ВВЕДИТЕ КОМАНДУ]");
            command = scanner.nextLine();

            execute(command);
        }
    }

    private void execute(final String command) {
        if (HelpUtil.stringIsEmpty(command)) {
            return;
        }

        AbstractCommand abstractCommand = commandMap.get(command);

        if (abstractCommand == null) {
            System.out.println("[НЕИЗВЕСТНАЯ КОМАНДА]");
            return;
        }

        abstractCommand.execute();
    }

    public TaskController getTaskController() {
        return taskController;
    }

    public ProjectController getProjectController() {
        return projectController;
    }


    public Scanner getScanner() {
        return scanner;
    }

    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }
}
