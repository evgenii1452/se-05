package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Project;

import java.util.HashMap;
import java.util.Map;

public class ProjectRepository {
    private final Map<String, Project> projects = new HashMap<>();

    public void removeAll() {
        projects.clear();
    }

    public String persist(final Project project) {
        projects.put(project.getId() , project);

        return project.getId();
    }

    public Map<String, Project> findAll() {
        return projects;
    }

    public Project remove(final String id) {
        return projects.remove(id);
    }

    public Project find(final String id) {
        return projects.get(id);
    }

    public void merge(Project project) {
        if (projects.containsKey(project.getId())) {
            projects.replace(project.getId(), project);
        } else {
            projects.put(project.getId(), project);
        }
    }

    public boolean keyExists(final String key) {
        return projects.containsKey(key);
    }
}
