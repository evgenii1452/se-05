package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Task;

import java.util.*;

public class TaskRepository {
    private final Map<String, Task> tasks = new HashMap<>();


    public String persist(final Task task) {
        tasks.put(task.getId(), task);

        return task.getId();
    }

    public Map<String, Task> findAll(final String projectId) {
        Map<String, Task> projectTasks = new HashMap<>();

        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getProjectId().equals(projectId)) {
                projectTasks.put(task.getKey(), task.getValue());
            }
        }

        return projectTasks;
    }

    public Task remove(final String id) {
        return tasks.remove(id);
    }

    public void removeALl(String projectId) {
        Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Task> taskEntry = it.next();

            if (taskEntry.getKey().equals(projectId)) {
                it.remove();
            }
        }
    }

    public Task find(final String id) {
        return tasks.get(id);
    }

    public void merge(final Task task) {
        if (tasks.containsKey(task.getId())) {
            tasks.replace(task.getId(), task);
        } else {
            tasks.put(task.getId(), task);
        }
    }

    public boolean keyExists(final String key) {
        return tasks.containsKey(key);
    }
}
