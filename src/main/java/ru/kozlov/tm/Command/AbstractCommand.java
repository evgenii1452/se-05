package ru.kozlov.tm.Command;

import ru.kozlov.tm.Bootstrap.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();
}
