package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class ProjectGenerateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.PROJECT_GENERATE;
    }

    @Override
    public String getDescription() {
        return "Project generation";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().generate();
        System.out.println("[ПРОЕКТЫ СГЕНЕРИРОВАНЫ]");
    }
}
