package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.PROJECT_REMOVE;
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().showAll();

        System.out.println("Введите id проекта:");
        final String id = bootstrap.getScanner().nextLine();

        bootstrap.getProjectController().remove(id);
    }
}
