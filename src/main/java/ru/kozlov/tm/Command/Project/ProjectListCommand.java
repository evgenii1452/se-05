package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.PROJECT_LIST;
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().showAll();
    }
}
