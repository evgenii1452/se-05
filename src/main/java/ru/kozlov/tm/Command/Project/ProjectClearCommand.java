package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.PROJECT_CLEAR;
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().removeAll();

        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }
}
