package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.TASK_REMOVE;
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() {
        System.out.println("Введите id задачи:");
        final String id = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().remove(id);
    }
}
