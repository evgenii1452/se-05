package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class TaskGenerateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.TASK_GENERATE;
    }

    @Override
    public String getDescription() {
        return "Task generation";
    }

    @Override
    public void execute() {
        bootstrap.getTaskController().generate();
        System.out.println("[ЗАДАЧИ СГЕНЕРИРОВАНЫ]");
    }
}
