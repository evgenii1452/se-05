package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.TASK_LIST;
    }

    @Override
    public String getDescription() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().showAll();

        System.out.println("Введите id проекта:");
        final String  projectId = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().showAll(projectId);
    }
}
