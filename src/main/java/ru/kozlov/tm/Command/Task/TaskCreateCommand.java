package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.TASK_CREATE;
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public void execute() {
        bootstrap.getProjectController().showAll();

        System.out.println("Введите id проекта:");
        final String projectId = bootstrap.getScanner().nextLine();

        System.out.println("Введите название задачи:");
        final String taskName = bootstrap.getScanner().nextLine();

        System.out.println("Введите описание задачи:");
        final String taskDescription = bootstrap.getScanner().nextLine();

        System.out.println("Введите дату начала задачи в формате \"30/12/1900\"");
        final String dateStart = bootstrap.getScanner().nextLine();

        System.out.println("Введите дату завершения задачи в формате \"30/12/1900\"");
        final String dateEnd = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().create(projectId, taskName, taskDescription, dateStart, dateEnd);
    }
}
