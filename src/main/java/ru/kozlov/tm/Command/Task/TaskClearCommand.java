package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.TASK_CLEAR;
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта:");
        final String projectId = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().removeAll(projectId);
    }
}
