package ru.kozlov.tm.Command.Basic;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

import java.util.Map;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.HELP;
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (Map.Entry<String, AbstractCommand> commandEntry: bootstrap.getCommandMap().entrySet()){
            System.out.println(commandEntry.getKey() + ": " + commandEntry.getValue().getDescription());
        }
    }
}
