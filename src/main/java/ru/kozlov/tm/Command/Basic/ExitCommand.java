package ru.kozlov.tm.Command.Basic;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;

public class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return Command.EXIT;
    }

    @Override
    public String getDescription() {
        return "Exit task manager";
    }

    @Override
    public void execute() {
        System.out.println("*** GOODBYE ***");
    }
}
